<?php

use App\Models\City;

beforeEach(function () {
    $this->cities = City::all();
    $this->citiesArray = $this->cities->toArray();
});

it('get all cities list', function () {
    expect($this->citiesArray[0])->toHaveKeys(["id", "name", "code", "country"]);
    expect($this->citiesArray)->toBeArray();
});

it('get 19 cities', function () {
    expect($this->citiesArray)->toHaveCount(19);
});
