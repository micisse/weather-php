<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cities = [
            "Toulouse" => 31000,
            "Paris" => 75000,
            "Lille" => 59000,
            "Marseille" => 13001,
            "Lyon" => 69001,
            "Bordeaux" => 33000,
            "Nantes" => 44000,
            "Nice" => 06000,
            "Montpellier" => 34000,
            "Toulon" => 83000,
            "Narbonne" => 11100,
            "Perpignan" => 66000,
            "Pau" => 64000,
            "Poitiers" => 86000,
            "Limoges" => 87000,
            "Orléans" => 45000,
            "Versailles" => 78000,
            "Dijon" => 21000,
            "Clermont-Ferrand" => 63000
        ];
        $country = "France";

        foreach ($cities as $cityName => $cityCode) {
            \App\Models\City::create([
                "name" => $cityName,
                "code" => $cityCode,
                "country" => $country,
            ]);
        }
    }
}
