<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeatherController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(["middleware" => ["web", "locale.switcher"]], function () {
    Route::get("/", [WeatherController::class, "index"])->name("index.homepage");

    /* -------------------------------------------------------------------------- */

    Route::get("/locale/{locale}", [WeatherController::class, "setLocale"])
        ->name("index.locale");

    /* -------------------------------------------------------------------------- */

    Route::post("/city-name/{cityName}", [WeatherController::class, "getDataByCity"])
        ->name("index.city-name");

    /* -------------------------------------------------------------------------- */

    // Route::post("/api-key", [WeatherController::class, "setApiKey"])
    //     ->name("index.set-api-key");
});
