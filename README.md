# Weather PHP With Laravel

**PHP ^8.2.2** / **Laravel v10.48.4**

### PREREQUISITES

- **PHP**: *>= 8.1*
- **Docker**
- **Node v20.8.0**
- **+ Yarn v3.6.4 or NPM v10.1.0**
- See here for more: https://laravel.com/docs/10.x/deployment

### STACK

PHP v8.2, Laravel v10.x, Docker (*PHPMyAdmin,Mysql,Redis*), Vite (Build), Pint, [Pest](https://pestphp.com/), SCSS.

### SETUP

#### Openweathermap

You need a api key from https://openweathermap.org/

#### Docker (mysql/phpmyadmin/redis)

*Please, ensure that you have docker and docker-compose on your machine before*

##### Step 1: Config

Update or replace env vars in <[rootDir]>/docker/docker-compose.yml under *environement* key

##### Step 2: Up docker container
```sh
$ cd docker
$ docker-compose up -d
```
##### Port is not default to avoid collision with native installation (on my system by example)

- **phpmyadmin** is up here http://localhost:8082
- **redis** is up on port 6380
- **mysql** is up on port 3307

![docker ps -a](docker/docker-ps.png)

##### Step 3: Migrate and seed cities data
```sh
$ php artisan db:seed
```
```sh
// Refresh the database and run all database seeds (Optionnal)
$  php artisan migrate:refresh --seed
```

#### Copy & update

* Copy and rename **.env.example** to **.env**
* Update vars in .env file (<[rootDir]>/.env ["**DB_DATABASE=**", "**DB_USERNAME=**", "**DB_PASSWORD=**", **OPENWEATHERMAP_KEY=**, "**REDIS_PORT=**"]). The values must be the same as the one you placed in the docker-compose file (PORT, DB...) to avoid issue.

##### If you don't use docker, connect your own mysql, phpmyadmin or redis and update .env file in <[rootDir]>.

#### Install deps

```sh
$ composer install
$ yarn install
```

#### Set the application key if not

```sh
$ php artisan key:generate
```

#### Start server

```sh
$ php artisan serve
```
```sh
$ yarn dev
```

> Open: http://127.0.0.1:8000

#### Build assets

```sh
$ yarn build (once for production)
```

#### Run tests (with Pest, not PHPUnit)

```sh
$ ./vendor/bin/pest
```

---

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>
