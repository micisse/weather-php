<?php

return [

    "precipitation" => "Précipitation",
    "humidity" => "Humidité",
    "wind" => "Vent",
    "use_your_api_key" => "Utiliser votre clé d'API",
    "api_key" => "Clé d'API",
    "use_default_api_key" => "Utiliser l'ancienne",
    "api_limit" => "Cette application utilise un service externe avec un plan gratuit et donc avec des appels limités. Si vous voyez ce message, cela signifie que le nombre d'appels vers cette API a été atteint. Utilisez le champ juste au-dessus pour utiliser directement votre clé. Créez un compte ici https://openweathermap.org/ pour l'occasion, vous pourrez le supprimer après vos tests.",
    /* -------------------------------------------------------------------------- */
    /*                                 DAY OF WEEK                                */
    /* -------------------------------------------------------------------------- */
    "sunday" => "Dimanche",
    "monday" => "Lundi",
    "tuesday" => "Mardi",
    "wednesday" => "Mercredi",
    "thursday" => "Jeudi",
    "friday" => "Vendredi",
    "saturday" => "Samedi",
    "sunday_abbr" => "Dim.",
    "monday_abbr" => "Lun.",
    "tuesday_abbr" => "Mar.",
    "wednesday_abbr" => "Mer.",
    "thursday_abbr" => "Jeu.",
    "friday_abbr" => "Ven.",
    "saturday_abbr" => "Sam.",
    /* -------------------------------------------------------------------------- */
    /*                                   MONTHS                                   */
    /* -------------------------------------------------------------------------- */
    "january" => "Janvier",
    "february" => "Février",
    "march" => "Mars",
    "april" => "Avril",
    "may" => "Mai",
    "june" => "Juin",
    "july" => "Juillet",
    "august" => "Aout",
    "september" => "Septembre",
    "october" => "Octobre",
    "november" => "Novembre",
    "december" => "Décembre",
    "january_abbr" => "Janv.",
    "february_abbr" => "Févr.",
    "march_abbr" => "Mars",
    "april_abbr" => "Avr.",
    "may_abbr" => "Mai",
    "june_abbr" => "Juin",
    "july_abbr" => "Juill.",
    "august_abbr" => "Août",
    "september_abbr" => "Sept.",
    "october_abbr" => "Oct.",
    "november_abbr" => "Nov.",
    "december_abbr" => "Déc.",

];
