@php
$currentLocale = app()->getLocale();
@endphp

<div class="btn-group locale">
    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown"
        aria-expanded="false">
        {{ strtoupper($currentLocale) }}
    </button>
    <ul class="dropdown-menu">
        <li>
            <a class="dropdown-item {{ $currentLocale === 'fr' ? 'disabled' : '' }}"
                href="{{ route('index.locale', 'fr') }}">
                FR
            </a>
        </li>
        <li>
            <a class="dropdown-item {{ $currentLocale === 'en' ? 'disabled' : '' }}"
                href="{{ route('index.locale', 'en') }}">
                EN
            </a>
        </li>
    </ul>
</div>
