@extends('layouts.index')

@section('page_title', $cityName)
@section('weather_class', $weatherClass)

@section('content')

    <div class="weather-side {{ $weatherClass }}">
        <div class="date-container">
            <h2 class="date-dayname">{{ $dayName }}</h2>
            <span class="date-day">{{ $date }}</span>
            <i class="location-icon" data-feather="map-pin"></i>
            <span class="location">
                {{ $cityName }}, {{ $countryCode }}
            </span>
        </div>
        <div class="weather-container">
            <i class="weather-icon" data-feather="{{ $iconName }}"></i>
            <h1 class="weather-temp">{{ $current->temp }}°C</h1>
            <h3 class="weather-desc">{{ ucfirst($weather->description) }}</h3>
        </div>
    </div>
    <div class="info-side">
        <div class="today-info-container">
            <div class="today-info">
                <div class="precipitation">
                    <span class="title">{{ strtoupper(__('weather.precipitation')) }}</span>
                    <span class="value">
                        {{ $precipitation }}
                    </span>
                    <div class="clear"></div>
                </div>
                <div class="humidity">
                    <span class="title">{{ strtoupper(__('weather.humidity')) }}</span>
                    <span class="value">
                        {{ $current->humidity }} %
                    </span>
                    <div class="clear"></div>
                </div>
                <div class="wind">
                    <span class="title">{{ strtoupper(__('weather.wind')) }}</span>
                    <span class="value">
                        {{ $current->wind_speed }} km/h
                    </span>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="week-container">
            <ul class="week-list">
                <div class="row">
                    @foreach ($forecastData as $data)
                        <div class="col">
                            <li class="{{ $data->active }} {{ $current->dt === $data->dt ? 'current' : '' }}">
                                <i class="day-icon" data-feather="{{ $data->iconName }}"></i>
                                <span class="day-name">{{ $data->dayName }}</span>
                                <span class="day-temp">{{ $data->temp }}°C</span>
                            </li>
                        </div>
                    @endforeach
                    <div class="clear"></div>
                </div>
            </ul>
        </div>
        <div class="options-container">
            <div class="input-group dropup">
                <div class="btn btn-secondary city-name">
                    {{ $cityName }}
                </div>
                <button type="button" class="btn btn-secondary btn-sm dropdown-toggle dropdown-toggle-split"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    @foreach ($cities as $c)
                        @if ($c !== $cityName)
                            <li>
                                <a class="dropdown-item" href="{{ route('index.city-name', strtolower($c->name)) }}">
                                    {{ $c->name }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>

            @include("includes.locale")
        </div>
    </div>

@endsection
