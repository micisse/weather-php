<?php

use Carbon\Carbon;

if (!function_exists("getBodyClassName")) {
    /**
     * Get the weather icn
     */
    function getBodyClassName(string $key): string
    {
        $keys = [
            "Clear" => "clear-sky",
            "Clouds" => "cloud",
            "Rain" => "cloud-rain",
            "Drizzle" => "cloud-drizzle",
            "Thunderstorm" => "cloud-lightning",
            "Snow" => "cloud-snow",
            "Mist" => "wind",
            "Smoke" => "smoke",
            "Haze" => "haze",
            "Dust" => "dust",
            "Fog" => "fog",
            "Sand" => "sand",
            "Ash" => "ash",
            "Squall" => "squalls",
            "Tornado" => "tornado",
        ];

        return $keys[$key];
    }
}

if (!function_exists("getWeatherIcon")) {
    /**
     * Get the weather icn
     */
    function getWeatherIcon(string $key): string
    {
        $keys = [
            "Clear" => "sun",
            "Clouds" => "cloud",
            "Rain" => "cloud-rain",
            "Drizzle" => "cloud-drizzle",
            "Thunderstorm" => "cloud-lightning",
            "Snow" => "cloud-snow",
            "Mist" => "wind",
            "Smoke" => "wind",
            "Haze" => "wind",
            "Dust" => "wind",
            "Fog" => "wind",
            "Sand" => "wind",
            "Ash" => "wind",
            "Squall" => "wind",
            "Tornado" => "wind",
        ];

        return $keys[$key];
    }
}

if (!function_exists("apiURL")) {
    /**
     * Get API Weather data URL
     */
    function apiURL(array|string $q, string $apiKey, string $service = "weather"): string
    {
        $key = $apiKey ?? env("OPENWEATHERMAP_KEY");
        $locale = app()->getLocale();
        $baseURL = "https://api.openweathermap.org/data";
        $apiVersion = "2.5";

        if ($service === "weather") {
            return "{$baseURL}/{$apiVersion}/weather?q={$q}&lang={$locale}&units=metric&appid={$key}";
        } else {
            return "{$baseURL}/{$apiVersion}/onecall?lat={$q['lat']}&lon={$q['lon']}&exclude=hourly,alerts&lang={$locale}&units=metric&appid={$key}";
        }
    }
}

if (!function_exists("parseDate")) {
    /**
     * Parse date
     */
    function parseDate(int $timestamp)
    {
        return Carbon::parse($timestamp)->locale("en");
    }
}
