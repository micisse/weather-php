<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Support\Facades\Http;

class WeatherController extends Controller
{
    /**@var string */
    private static $apiKey;

    public string $countryName;
    public string $countryCode;
    public string $cityName;

    public function __construct()
    {
        static::$apiKey = env("OPENWEATHERMAP_KEY");
        $this->countryName = "France";
        $this->countryCode = "FR";
        $this->cityName = "Paris";

        $this->setCity();
    }

    /**
     * Entry point
     */
    public function index()
    {
        $data = $this->dataToView();

        return view("home", $data);
    }

    /**
     * Get cities list
     */
    public function getCities()
    {
        $cities = City::where("country", $this->countryName)->orderBy("name")->get();

        return $cities;
    }

    /**
     * Data to send to the view
     */
    public function dataToView(): array
    {
        $data = $this->getCoords($this->cityName);
        $coord = $data["coord"];
        $oneCallData = $this->getOneCallData($coord["lat"], $coord["lon"], $data["dt"] ?: null);
        $forecastData = json_decode($oneCallData["forecastData"]);
        $current = json_decode($oneCallData["current"]);
        $timestamp = $current->dt;
        $dateParsed = parseDate($timestamp);
        $monthName = __("weather." . strtolower($dateParsed->monthName) . "_abbr");
        $day = $dateParsed->day;
        $year = $dateParsed->year;
        $dayName = strtolower($dateParsed->dayName);
        /* ---------------------------------- DATE ---------------------------------- */
        $weather = $current->weather[0];
        $iconName = getWeatherIcon($weather->main);
        $weatherClass = getBodyClassName($weather->main);
        $precipitation = $oneCallData["precipitation"];

        return [
            "current" => $current,
            "iconName" => $iconName,
            "countryCode" => $this->countryCode,
            "weather" => $weather,
            "date" => "{$day} {$monthName} {$year}",
            "dayName" => __("weather.{$dayName}"),
            "cityName" => $this->cityName,
            "weatherClass" => $weatherClass,
            "precipitation" => $precipitation,
            "forecastData" => $forecastData,
            "cities" => $this->getCities(),
        ];
    }

    /**
     * Get current weather data for one location by city name
     */
    private function getCoords(string $q)
    {
        $response = Http::get(apiURL($q, static::$apiKey));
        $data = $response->json();
        $data = [
            "coord" => $data["coord"],
            "dt" => $data["dt"],
        ];

        return $data;
    }

    /**
     * Set app locale
     */
    public function setLocale(string $locale)
    {
        session(["locale" => $locale]);
        return redirect()->back();
    }

    /**
     * Set city name
     */
    private function setCity(string $cityName = null): void
    {
        if (!is_null($cityName)) {
            $this->cityName = $cityName;
        }
    }

    /**
     * Get data by city name
     */
    public function getDataByCity(string $city)
    {
        $this->setCity(ucfirst($city));
        return $this->index();
    }

    /**
     * Get forecast weather data
     * We need to use getCoords function before to get lat & lon dynamically
     */
    private function getOneCallData(int $lat, int $lon): array
    {
        $locale = session()->get("locale") ?? app()->getLocale();
        $response = Http::get(apiURL(["lat" => $lat, "lon" => $lon], static::$apiKey, "onecall"));
        $data = $response->json();
        $dailyData = $data["daily"];
        $lat = $data["lat"];
        $lon = $data["lon"];
        $keepDailyData = [];
        $minutelyData = $data["minutely"];
        $precipitation = $minutelyData[0]["precipitation"];
        $todayName = __("weather." . strtolower(now()->locale($locale)->dayName) . "_abbr");

        foreach ($dailyData as $d) {
            $timestamp = $d["dt"];
            $dateParsed = parseDate($timestamp);
            $dayName = __("weather." . strtolower($dateParsed->dayName) . "_abbr");

            if ($todayName !== $dayName && count($keepDailyData) < 4) {
                $weather = (array) $d["weather"][0];
                $iconName = getWeatherIcon($weather["main"]);
                $oneCallData = array_merge($d, [
                    "coord" => [
                        "lat" => $lat,
                        "lon" => $lon,
                    ],
                    "active" => $todayName === $dayName ? "active" : "",
                    "extrasClass" => "",
                    "dayName" => $dayName,
                    "temp" => $d["temp"]["day"],
                    "iconName" => $iconName,
                ]);
                $keepDailyData[] = $oneCallData;
            }
        }

        return [
            "forecastData" => json_encode($keepDailyData),
            "current" => json_encode($data["current"]),
            "precipitation" => $precipitation,
        ];
    }
}
